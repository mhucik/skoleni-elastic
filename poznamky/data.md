#Plnění dat
indexace
	- tokenizér - rozseká string na menší části
	- filtry - filtrují v datech tokenizéru
	- analyzér 
		- obálka nad tokenizérem a filtry
		- obaluje jeden tokenizér a jeden nebo více filtrů

Základní analyzér:
```rest
POST _analyze
{
  "text": "Na dnešní školení do PeckaDesign dorazilo 10 programátorů!",
  "tokenizer": "whitespace"
}
```

Vezme text a rozdělí ho podle mezer (tokenizer = whitespace)

výsledek:
```rest
{
  "tokens": [
      {
        "token": "Na",
        "start_offset": 0,
        "end_offset": 2,
        "type": "word",
        "position": 0
      },
      {
        "token": "dnešní",
        "start_offset": 3, //v analyzovaném stringu začíná slovo na pozici
        "end_offset": 9, //a končí na pozici
        "type": "word",
        "position": 1 //kolikáté slovo v analyzovaném stringu
      },
      {
        "token": "školení",
        "start_offset": 10,
        "end_offset": 17,
        "type": "word",
        "position": 2
      },
    .
    .
    .
    .
}
```


seznam tokenizéru https://www.elastic.co/guide/en/elasticsearch/reference/2.3/analysis-custom-analyzer.html (vpravo dole v menu)
