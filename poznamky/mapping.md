Dynamický mapping
- volí se největší datový typ, jaký je dostupný
	- například vkládám integer, ale elastic mu raději nastaví long
- umí si nastavit vlastní mapping, pokud mu defaultně nějaký nenastavím
 
 Příklad mapování:
 ```
 GET /product
 
{
  "product": {
    "aliases": {},
    "mappings": {
      "default": {
        "properties": {
          "availability": {
            "type": "long"
          },
          "createdAt": {
            "type": "date",
            "format": "strict_date_optional_time||epoch_millis"
          },
          "is_available": {
            "type": "boolean"
          },
          "name": {
            "type": "string"
          },
          "price": {
            "type": "string"
          }
        }
      }
    },
    "settings": {
      "index": {
        "creation_date": "1529652778253",
        "number_of_shards": "5",
        "number_of_replicas": "1",
        "uuid": "q0Ihc0VwTYWkyNtrItyxdQ",
        "version": {
          "created": "2040699"
        }
      }
    },
    "warmers": {}
  }
}
```

Statický mapping:
```rest
PUT product
{
	"mappings": {
		"default": { //nastavení typu
			"properties": {
				"name": {
					"type": "string"
		  		},
				"price": {
					"type": "float"
				},
				"is_available": {
					"type": "boolean"
				},
				"createdAt": {
					"type": "date"
				},
				"availability": {
					"type": "integer"
				}
			}
		}
	}
}
```

- field už nejde přetypovat!
- "sloupce" v mappingu již nejde smazat
- mapping je jen dokumentace, není to stejně jako v databázi
- nemusím tolik řešit to, co je v mappingu
- jde zakázat, aby nové typy netvořil automaticky, ale vyhodil chybu na při pokusu o vložení fieldu, který není v mappingu


Pokročilé mapování
```rest
PUT products
{
  "mappings": {
      "default": {
        "properties": {
          "active": {
            "type": "boolean"
          },
          "availability": {
            "type": "long"
          },
          "category": {
            "type": "string"
          },
          "createdAt": {
            "type": "date",
            "format": "strict_date_optional_time||epoch_millis"
          },
          "id": {
            "type": "long"
          },
          "name": {
            "type": "string",
            "fields": {//můžu nastavit více typů fieldu
              "raw": {
                "type": "string"
              },
              "ascii": {
                "type": "string",
                "analyzer": "my_lowercase_ascii_analyzer", //nastavení vkládacího analyzéru
                "search_analyzer": "standard" //nastavení vyhledávácího anaylzéru, pokud není vyplněn, použije se pro vyhledávání nastavený analizer v klíči "analyzer"
              }
            }
          },
          "price": {
            "type": "double"
          }
        }
      }
    }
}
```

```rest

GET products/_search
{
  "query": {
    "multi_match": {
      "query": "modrý",
      "fields": ["name^3", "name.ascii"]//prioritizuji diakritiku a až potom bez diakritiky
    }
  }
}
```
