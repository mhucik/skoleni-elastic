#Základní operace

 - url: http://localhost:5601/app/sense
 - server: http://elasticsearch:9200
 
##Vytvoření indexu
		index		typ		identifikátor
	PUT	product/	default/	12

 - identifikátor je fajn mít stejný jako v databázi

Vložení hodnoty do ES
```
PUT product/default/1
{
  "name": "Iphone 7",
  "price": "12222",
  "is_available": true,
  "createdAt": "2017-08-19",
  "availability": 5
}
```

```yaml
GET product/default/1

{
  "_index": "product",
  "_type": "default",
  "_id": "1",
  "_version": 2,
  "found": true,
  "_source": {
    "name": "Iphone 7",
    "price": "12222",
    "is_available": true,
    "createdAt": "2017-08-19",
    "availability": 5
  }
}

```

- `_version` je verzo daného záznamu pod jedním `_id`

- Smazání indexu: `DELETE product`

Pokud zkusím:
```rest
POST product/default/1
{
  "is_available": false
}
```
smaže mi to všechny fieldy, musím tedy odesílat všechny fieldy, i když aktualizuju pouze jeden field

Překlepové vyhledávání
```rest
GET products/_search
{
 "query": {
   "match": {
     "name": {
       "query": "iphone sedy",
       "operator": "and",
       "fuzziness": 1 //překlep - dovolí nahradit 1 znak za jakýkoliv jiný znak
     }
   }
 } 
```

Podmíněné vyhledávání
```rest
GET products/_search
{
  "query": {
    "bool": {
      "should": [
        {
          "match": {
            "name": "iphone"
          }
        },
        {
          "match": {
            "category": "iphone"
          }
        },
        {
          "term": {
            "availability": {
              "value": 0
            }
          }
        }
      ],
      "minimum_number_should_match": 2 //musí platit alespoň dvě podmínky
    }
  }
}
```


```rest
GET products/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "name": "iphone"
          }
        },
        {
          "range": {
            "price": {
              "gte": 15000
            }
          }
        }
      ],
      "filter": { //aplikuje se nejdřív, abychom si mohli vyfiltrovat co nejvíce dat a teprve na tento výsledek použít nějaké složitější filtrování (má za úkol zúžit množinu dat, ve kterých se má vyhledávat)
        "term": {
          "active": true
        }
      }
    }
  }
}
```
